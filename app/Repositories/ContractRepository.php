<?php

namespace App\Repositories;

use App\Models\Contract;
use App\Repositories\BaseRepository;

/**
 * Class ContractRepository
 * @package App\Repositories
 * @version March 6, 2022, 6:16 am UTC
*/

class ContractRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'employee_id',
        'position_id',
        'date_start',
        'date_end'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Contract::class;
    }
}
