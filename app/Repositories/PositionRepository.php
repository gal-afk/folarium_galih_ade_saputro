<?php

namespace App\Repositories;

use App\Models\Position;
use App\Repositories\BaseRepository;

/**
 * Class PositionRepository
 * @package App\Repositories
 * @version March 6, 2022, 6:05 am UTC
*/

class PositionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Position::class;
    }

    public static function getList()
    {
        $data = Position::all()->toArray();
        $arr['']= 'Pilih jabatan...';
        foreach($data as $val){
            $arr[$val['id']] = $val['title'];
        }
        return $arr;
    }
}
