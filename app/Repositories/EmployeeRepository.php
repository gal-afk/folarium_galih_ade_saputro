<?php

namespace App\Repositories;

use App\Models\Employee;
use App\Repositories\BaseRepository;

/**
 * Class EmployeeRepository
 * @package App\Repositories
 * @version March 6, 2022, 4:46 am UTC
*/

class EmployeeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'nik',
        'alamat',
        'jen_kel'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Employee::class;
    }

    public static function getList()
    {
        $data = Employee::doesntHave('contract')->get()->toArray();
        $arr['']= 'Pilih karyawan...';
        foreach($data as $val){
            $arr[$val['id']] = $val['name'];
        }

        return $arr;
    }

}
