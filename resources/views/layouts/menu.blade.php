<li class="side-menus">
    <a class="nav-link" href="/">
        <i class=" fas fa-building"></i><span>Dashboard</span>
    </a>
</li>

{{--<li class="dropdown">--}}
{{--    <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>User Management</span></a>--}}
{{--    <ul class="dropdown-menu" style="display: block;">--}}
{{--        <li class="{{ Request::is('permissions*') ? 'active' : '' }} nav-item">--}}
{{--            <a href="{{ route('permissions.index') }}"><i class="fa fa-edit"></i><span>@lang('models/permissions.plural')</span></a>--}}
{{--        </li>--}}

{{--        <li class="{{ Request::is('roles*') ? 'active' : '' }}">--}}
{{--            <a href="{{ route('roles.index') }}"><i class="fa fa-edit"></i><span>@lang('models/roles.plural')</span></a>--}}
{{--        </li>--}}

{{--        <li class="{{ Request::is('users*') ? 'active' : '' }}">--}}
{{--            <a href="{{ route('users.index') }}"><i class="fa fa-edit"></i><span>@lang('models/users.plural')</span></a>--}}
{{--        </li>--}}
{{--    </ul>--}}
{{--</li>--}}


<li class="{{ Request::is('employees*') ? 'active' : '' }}">
    <a href="{{ route('employees.index') }}"><i class="fa fa-edit"></i><span>@lang('models/employees.plural')</span></a>
</li>

<li class="{{ Request::is('positions*') ? 'active' : '' }}">
    <a href="{{ route('positions.index') }}"><i class="fa fa-edit"></i><span>@lang('models/positions.plural')</span></a>
</li>

<li class="{{ Request::is('contracts*') ? 'active' : '' }}">
    <a href="{{ route('contracts.index') }}"><i class="fa fa-edit"></i><span>@lang('models/contracts.plural')</span></a>
</li>

