// datatable css
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.bootstrap.css">

<style>
    table.dataTable thead .sorting_asc, table.dataTable thead .sorting, table.dataTable thead .sorting_desc {
        background-image: none !important;
    }
    table.dataTable thead th input{
        display: block;
        width: 90%;
        margin-right: 5px;
    }
    .table thead th {
        vertical-align: top !important;
    }
    .dataTables_filter{
        display: none;
    }

</style>

