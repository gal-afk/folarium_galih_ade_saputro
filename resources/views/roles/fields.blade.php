<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/roles.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Guard Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('guard_name', __('models/roles.fields.guard_name').':') !!}
    {!! Form::text('guard_name', 'web', ['class' => 'form-control','readonly']) !!}
</div>

<div class="form-group col-sm-12">
    <div class="card">
        <div class="card-header bg-secondary tx-white" style="min-height: 50px!important;">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" name="checkAll" id="checkAll">
                <label class="custom-control-label" for="checkAll"> Check All Permissions</label>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                @foreach ($permissions as $permission)
                    <div class="col-sm-3">
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('permissions[]',  $permission->id, @$role->permissions, ['class' => 'custom-control-input', 'id' => 'customCheck' . $loop->index] ) }}
                            {{Form::label('customCheck' . $loop->index, ucfirst($permission->name), ['class' => 'custom-control-label']) }}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('roles.index') }}" class="btn btn-light">Cancel</a>
</div>

@section('script')
<!-- Relational Form table -->
<script>

	$(document).ready(function() {
        var checkbox = $('input[name="permissions[]"]').length;
        if($('input[name="permissions[]"]:checked').length == checkbox)
        { $('#checkAll').prop('checked', true); }
    });

	$(document).on("change", "#checkAll", function() {
		if($(this).is(':checked')) {
			$('input[name="permissions[]"]:checkbox').prop('checked', true);
		}
		else {
			$('input[name="permissions[]"]:checkbox').prop('checked', false);
		}
	});

    $(document).on('change', 'input[name="permissions[]"]', function() {
        var checkbox = $('input[name="permissions[]"]').length;
		if($(this).is(':checked')) {
			if($('input[name="permissions[]"]:checked').length == checkbox)
                $('#checkAll').prop('checked', true);
            else
                $('#checkAll').prop('checked', false);
		}
		else {
			$('#checkAll').prop('checked', false);
		}
	});

</script>
<!-- End Relational Form table -->
@endsection

