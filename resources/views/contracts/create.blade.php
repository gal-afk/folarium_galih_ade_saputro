@extends('layouts.app')
@section('title')
    Create @lang('models/contracts.singular')
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading m-0">Create @lang('models/contracts.singular')</h3>
            <div class="filter-container section-header-breadcrumb row justify-content-md-end">
                <a href="{{ route('contracts.index') }}" class="btn btn-primary">Back</a>
            </div>
        </div>
        <div class="content">
            @include('stisla-templates::common.errors')
            <div class="section-body">
               <div class="row">
                   <div class="col-lg-12">
                       <div class="card">
                           <div class="card-body ">
                                {!! Form::open(['route' => 'contracts.store','id' => 'form']) !!}
                                    <div class="row">
                                        @include('contracts.fields')
                                        <div class="form-group col-sm-12">
                                            {{--    {!! Form::submit('Save', ['class' => 'btn btn-primary btn-save']) !!}--}}
                                            <a href="javascript:;" class="btn btn-primary btn-save">Save</a>
                                            <a href="{{ route('contracts.index') }}" class="btn btn-light">Cancel</a>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                           </div>
                       </div>
                   </div>
               </div>
            </div>
        </div>
    </section>
@endsection

