<!-- Employee Id Field -->
<div class="form-group">
    {!! Form::label('employee_id', __('models/contracts.fields.employee_id').':') !!}
    <p>{{ $contract->employee_id }}</p>
</div>

<!-- Position Id Field -->
<div class="form-group">
    {!! Form::label('position_id', __('models/contracts.fields.position_id').':') !!}
    <p>{{ $contract->position_id }}</p>
</div>

<!-- Date Start Field -->
<div class="form-group">
    {!! Form::label('date_start', __('models/contracts.fields.date_start').':') !!}
    <p>{{ $contract->date_start }}</p>
</div>

<!-- Date End Field -->
<div class="form-group">
    {!! Form::label('date_end', __('models/contracts.fields.date_end').':') !!}
    <p>{{ $contract->date_end }}</p>
</div>

