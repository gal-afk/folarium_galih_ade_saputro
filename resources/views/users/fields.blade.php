<!-- Name Field -->
<div class="row">
    <div class="form-group col-sm-6">
        <div class="row">
            <div class="form-group col-sm-12">
                {!! Form::label('name', __('models/users.fields.name').':') !!}
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>
            
            <!-- Email Field -->
            <div class="form-group col-sm-12">
                {!! Form::label('email', __('models/users.fields.email').':') !!}
                {!! Form::email('email', null, ['class' => 'form-control']) !!}
            </div>
            
            {{-- <!-- Email Verified At Field -->
            <div class="form-group col-sm-12">
                {!! Form::label('email_verified_at', __('models/users.fields.email_verified_at').':') !!}
                {!! Form::date('email_verified_at', null, ['class' => 'form-control','id'=>'email_verified_at']) !!}
            </div> --}}
            
            @push('scripts')
                <script type="text/javascript">
                    $('#email_verified_at').datetimepicker({
                        format: 'YYYY-MM-DD HH:mm:ss',
                        useCurrent: false
                    })
                </script>
            @endpush
            
            <!-- Password Field -->
            <div class="form-group col-sm-12">
                {!! Form::label('password', __('models/users.fields.password').':') !!}
                {!! Form::password('password', ['class' => 'form-control']) !!}
            </div>
            
            {{-- <!-- Remember Token Field -->
            <div class="form-group col-sm-12">
                {!! Form::label('remember_token', __('models/users.fields.remember_token').':') !!}
                {!! Form::text('remember_token', null, ['class' => 'form-control']) !!}
            </div> --}}
        </div>
    </div>
    <div class="form-group col-sm-6">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header bg-secondary tx-white" style="min-height: 50px!important;">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="checkAll" id="checkAll">
                            <label class="custom-control-label" for="checkAll"> Roles</label>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group col-sm-12 p-0">
                            @foreach ($roles as $role)
                                <div class="custom-control custom-checkbox">
                                    {!! Form::checkbox('roles[]',   $role->id, !empty($user->roles) ? $user->roles : null, ["class" => "custom-control-input", "id" => "customCheck" . $loop->index] ) !!}
                                    {!! Form::label("customCheck" . $loop->index, $role->name, ["class" => "custom-control-label"]) !!}
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>




<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('users.index') }}" class="btn btn-light">Cancel</a>
</div>


@section('script')
<!-- Relational Form table -->
<script>
	$(document).ready(function()
	{
        var checkbox = $('input[name="roles[]"]').length;
        if($('input[name="roles[]"]:checked').length == checkbox)
        { $('#checkAll').prop('checked', true); }
    });

	$(document).on("change", "#checkAll", function() {
		if($(this).is(':checked')) {
			$('input[name="roles[]"]:checkbox').prop('checked', true);
		}
		else {
			$('input[name="roles[]"]:checkbox').prop('checked', false);
		}
	});

    $(document).on('change', 'input[name="roles[]"]', function() {
        var checkbox = $('input[name="roles[]"]').length;
		if($(this).is(':checked')) {
			if($('input[name="roles[]"]:checked').length == checkbox)
                $('#checkAll').prop('checked', true);
            else
                $('#checkAll').prop('checked', false);
		}
		else {
			$('#checkAll').prop('checked', false);
		}
	});

</script>
<!-- End Relational Form table -->
@endsection
