<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', __('models/positions.fields.title').':') !!}
    <p>{{ $position->title }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', __('models/positions.fields.description').':') !!}
    <p>{{ $position->description }}</p>
</div>

