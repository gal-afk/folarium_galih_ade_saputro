<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', __('models/positions.fields.title').':') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', __('models/positions.fields.description').':') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

{{--<!-- Submit Field -->--}}
{{--<div class="form-group col-sm-12">--}}
{{--    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}--}}
{{--    <a href="{{ route('positions.index') }}" class="btn btn-light">Cancel</a>--}}
{{--</div>--}}

@section('script')
    <script>

        $(document).ready(function() {
            $('.select2').select2();

        });

        $('.btn-save').on('click', function(e) {

            $.ajax({
                type: "POST",
                url: "{{ route('positions.store') }}",
                enctype: 'multipart/form-data',
                data: new FormData(document.getElementById("form")),
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    if (response.status == 'success') {
                        alert('Jabatan berhasil disimpan');
                        window.location.href = "{{ route('positions.index') }}";
                    } else {
                        alert('Semething went wrong');
                    }
                },
                error: function(error) {}
            });
        });

    </script>

@endsection
