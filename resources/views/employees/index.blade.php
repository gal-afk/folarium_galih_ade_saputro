@extends('layouts.app')
@section('title')
     @lang('models/employees.plural')
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>@lang('models/employees.plural')</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('employees.create')}}" class="btn btn-primary form-btn">Create <i class="fas fa-plus"></i></a>
            </div>
        </div>
    <div class="section-body">
        @include('stisla-templates::common.errors')
        @include('flash::message')
       <div class="card">
            <div class="card-body">
                @include('employees.table')
            </div>
       </div>
   </div>

    </section>
@endsection



