<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/employees.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Nik Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nik', __('models/employees.fields.nik').':') !!}
    {!! Form::number('nik', null, ['class' => 'form-control']) !!}
</div>

<!-- Alamat Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('alamat', __('models/employees.fields.alamat').':') !!}
    {!! Form::textarea('alamat', null, ['class' => 'form-control']) !!}
</div>

<!-- Jen Kel Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jen_kel', __('models/employees.fields.jen_kel').':') !!}
    {!! Form::select('jen_kel',['laki-laki' => 'laki-laki','perempuan' => 'perempuan'] ,null, ['class' => 'form-control select2']) !!}
</div>

{{--<!-- Submit Field -->--}}
{{--<div class="form-group col-sm-12">--}}
{{--    {!! Form::submit('Save', ['class' => 'btn btn-primary btn-save']) !!}--}}
{{--    <a href="javascript:;" class="btn btn-primary btn-save">Save</a>--}}
{{--    <a href="{{ route('employees.index') }}" class="btn btn-light">Cancel</a>--}}
{{--</div>--}}

@section('script')
<script>

    $(document).ready(function() {
        $('.select2').select2();

    });

    $('.btn-save').on('click', function(e) {

        $.ajax({
            type: "POST",
            url: "{{ route('employees.store') }}",
            enctype: 'multipart/form-data',
            data: new FormData(document.getElementById("form")),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: function(response) {
                if (response.status == 'success') {
                    alert('Karyawan berhasil disimpan');
                    window.location.href = "{{ route('employees.index') }}";
                } else {
                    alert('Semething went wrong');
                }
            },
            error: function(error) {}
        });
    });

</script>

@endsection
