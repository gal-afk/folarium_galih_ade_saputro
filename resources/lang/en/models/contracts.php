<?php

return array (
  'singular' => 'Kontrak',
  'plural' => 'Kontrak',
  'fields' =>
  array (
    'id' => 'Id',
    'employee_id' => 'Karyawan',
    'position_id' => 'Jabatan',
    'date_start' => 'Date Start',
    'date_end' => 'Date End',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
