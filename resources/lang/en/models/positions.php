<?php

return array (
  'singular' => 'Jabatan',
  'plural' => 'Jabatan',
  'fields' =>
  array (
    'id' => 'Id',
    'title' => 'Title',
    'description' => 'Description',
    'created_at' => 'Created At',
    'deleted_at' => 'Deleted At',
    'updated_at' => 'Updated At',
  ),
);
